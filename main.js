const { app,BrowserView, BrowserWindow} = require('electron')
const path = require('path')
app.disableHardwareAcceleration()

let pyProc = null

const createPyProc = () => {
    let port = '4242'
    let script = path.resolve(__dirname, "teaMedia.exe -port 9091")
    // if (process.env.NODE_ENV === 'production') {
    //     script = path.join(process.resourcesPath, 'app.asar.unpacked/go', isWin ? 'testGo.exe' : 'testGo')
    // }
    console.log(script)
    pyProc = require('child_process').execFile(script, [port]) // 开辟一个子进程运行go打包出来的可执行程序
    if (pyProc != null) {
        console.log('child process success')
    }
}

const exitPyProc = () => {
    pyProc.kill()
    pyProc = null
}

app.whenReady().then(() => {
    createPyProc()
    const win = new BrowserWindow({ width: 1920, height: 1080,  frame: true })
    // Menu.setApplicationMenu(null) // 隐藏菜单栏
    win.setTitle('名师工作室服务软件')
    const view = new BrowserView()
    win.setBrowserView(view)
    view.setBounds({ x: 0, y: 0, width: 1920, height: 1080 })
    // // 这里的公众号ID从配置中来，首先看用户是从哪个公众号进行下载的
    // // 可以在这个页面上进行公众号切换，切换后把id保存到配置文件中
    view.webContents.loadURL('https://tea.ynmdiot.com/pc/31?desk=1')
})

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        exitPyProc()
        app.quit()
    }
})